FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=0.31.0


RUN mkdir -p /app/code
WORKDIR /app/code

RUN wget -L https://github.com/redlib-org/redlib/releases/download/v${VERSION}/redlib 
RUN chmod +x redlib

RUN adduser --home /nonexistent --no-create-home --disabled-password redlib
USER redlib

# Tell Docker to expose port 8080
EXPOSE 8080

CMD ["/app/code/redlib"]
